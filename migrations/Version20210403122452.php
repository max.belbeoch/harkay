<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210403122452 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE content (id INT AUTO_INCREMENT NOT NULL, lang VARCHAR(2) NOT NULL, post_id INT DEFAULT NULL, mission_id VARCHAR(255) DEFAULT NULL, page_title VARCHAR(255) DEFAULT NULL, page_description LONGTEXT DEFAULT NULL, title VARCHAR(255) NOT NULL, short_content VARCHAR(1024) DEFAULT NULL, content LONGTEXT DEFAULT NULL, published TINYINT(1) DEFAULT NULL, created_at DATETIME NOT NULL, last_updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE mission CHANGE published_en published_en TINYINT(1) DEFAULT NULL, CHANGE published_es published_es TINYINT(1) DEFAULT NULL, CHANGE page_display_newsletter page_display_newsletter TINYINT(1) DEFAULT NULL, CHANGE page_display_post page_display_post TINYINT(1) DEFAULT NULL, CHANGE page_display_mission page_display_mission TINYINT(1) DEFAULT NULL, CHANGE page_display_map page_display_map TINYINT(1) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE content');
        $this->addSql('ALTER TABLE mission CHANGE published_en published_en INT DEFAULT NULL, CHANGE published_es published_es INT DEFAULT NULL, CHANGE page_display_newsletter page_display_newsletter INT DEFAULT NULL, CHANGE page_display_post page_display_post INT DEFAULT NULL, CHANGE page_display_mission page_display_mission INT DEFAULT NULL, CHANGE page_display_map page_display_map INT DEFAULT NULL');
    }
}
