<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210507141256 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE general (id INT AUTO_INCREMENT NOT NULL, page_title_fr VARCHAR(255) DEFAULT NULL, page_description_fr VARCHAR(255) DEFAULT NULL, page_title_en VARCHAR(255) DEFAULT NULL, page_description_en VARCHAR(255) DEFAULT NULL, page_title_es VARCHAR(255) DEFAULT NULL, page_description_es VARCHAR(255) DEFAULT NULL, legal_name VARCHAR(255) DEFAULT NULL, siret VARCHAR(50) DEFAULT NULL, country VARCHAR(125) DEFAULT NULL, city VARCHAR(125) DEFAULT NULL, zip_code INTEGER(10) DEFAULT NULL, address VARCHAR(255) DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, phone_number VARCHAR(20) DEFAULT NULL, donation_link VARCHAR(500) DEFAULT NULL, fb_link VARCHAR(500) DEFAULT NULL, twt_link VARCHAR(500) DEFAULT NULL, ig_link VARCHAR(500) DEFAULT NULL, yt_link VARCHAR(500) DEFAULT NULL, created_at DATETIME DEFAULT NULL, last_updated_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('INSERT INTO `general` (`id`, `page_title_fr`, `page_description_fr`, `page_title_en`, `page_description_en`, `page_title_es`, `page_description_es`, `legal_name`, `siret`, `country`, `city`, `zip_code`, `address`, `email`, `phone_number`, `donation_link`, `fb_link`, `twt_link`, `ig_link`, `yt_link`, `created_at`, `last_updated_at`, `deleted_at`) VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, "2021-05-07 00:00:00", "2021-05-07 00:00:00", NULL);');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE general');
    }
}
