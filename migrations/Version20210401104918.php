<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210401104918 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE mission ADD content_en LONGTEXT DEFAULT NULL, ADD content_es LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE post ADD content_en LONGTEXT DEFAULT NULL, ADD published_en INT DEFAULT NULL, ADD content_es LONGTEXT DEFAULT NULL, ADD published_es INT DEFAULT NULL, ADD page_title VARCHAR(255) DEFAULT NULL, ADD page_description LONGTEXT DEFAULT NULL, ADD page_display_newsletter INT DEFAULT NULL, ADD page_display_post INT DEFAULT NULL, ADD page_display_mission INT DEFAULT NULL, ADD page_display_map INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE mission DROP content_en, DROP content_es');
        $this->addSql('ALTER TABLE post DROP content_en, DROP published_en, DROP content_es, DROP published_es, DROP page_title, DROP page_description, DROP page_display_newsletter, DROP page_display_post, DROP page_display_mission, DROP page_display_map');
    }
}
