<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210401120513 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE post CHANGE published_en published_en TINYINT(1) DEFAULT NULL, CHANGE published_es published_es TINYINT(1) DEFAULT NULL, CHANGE page_display_newsletter page_display_newsletter TINYINT(1) DEFAULT NULL, CHANGE page_display_post page_display_post TINYINT(1) DEFAULT NULL, CHANGE page_display_mission page_display_mission TINYINT(1) DEFAULT NULL, CHANGE page_display_map page_display_map TINYINT(1) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE post CHANGE published_en published_en INT DEFAULT NULL, CHANGE published_es published_es INT DEFAULT NULL, CHANGE page_display_newsletter page_display_newsletter INT DEFAULT NULL, CHANGE page_display_post page_display_post INT DEFAULT NULL, CHANGE page_display_mission page_display_mission INT DEFAULT NULL, CHANGE page_display_map page_display_map INT DEFAULT NULL');
    }
}
