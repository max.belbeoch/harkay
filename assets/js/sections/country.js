export const initCountryInfos = () => {

    const section = document.querySelector('.section-country');
    if (section) {

        const buttons = section.querySelectorAll('.links button');
        const panels = section.querySelectorAll('.item');

        buttons.forEach(btn => {
            btn.addEventListener('click', e => {
                e.preventDefault();
                buttons.forEach(b => b.classList.remove('active'));
                btn.classList.add('active');
                panels.forEach(p => p.classList.remove('visible'));
                [...panels].find(p => p.dataset.target === btn.dataset.target).classList.add('visible');
            })
        })
    }
};
