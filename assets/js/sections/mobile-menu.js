export const initMobileMenu = () => {
    const btn = document.querySelector('.icon-menu');
    const menu = document.querySelector('.mobile-menu');
    let isOpen = false;
    if(btn && menu) {

        const close = () => {
            document.body.style.overflowY = 'auto';
            menu.classList.remove('open');
            isOpen = false;
        };

        const open = () => {
            document.body.style.overflowY = 'hidden';
            menu.classList.add('open');
            isOpen = true;
        };

        btn.addEventListener('click', e => {
            e.preventDefault();
            if(isOpen) {
                close()
            } else {
                open()
            }
        });

        menu.querySelector('.icon-close').addEventListener('click', e => {
            e.preventDefault();
            close()
        })
    }
};
