const send = form => {
  const el = form.closest('div');
  el.closest('div').querySelector('.d-none').classList.remove('d-none');
  el.remove()
};

export const initNewsletter = () => {

  document.querySelectorAll('.js-newsletter-form').forEach(form => {
    form.addEventListener('submit', e => {
      e.preventDefault();

      fetch(form.action, {
        method: "POST",
        mode: "no-cors",
        cache: "no-cache",
        credentials: "same-origin",
        headers: {
          "Content-Type": "form-data"
        },
        body: new FormData(form)
      });

      form.classList.add('d-none');
      const p = form.closest('div').querySelector('p');
      p.innerText = p.dataset.success

    })
  })

};