export const initLanguageSwitcher = () => {

    document.querySelectorAll('.lang-switcher.desktop').forEach(el => {
        el.addEventListener('click', e => {
            if (!el.classList.contains('open')) {
                e.preventDefault();
                el.classList.add('open')
            } else if (e.target.closest('a').classList.contains('active')) {
                e.preventDefault();
                el.classList.remove('open')
            }
        })
    })
};