const Flickity = require('flickity');

export const initSliderNews = () => {

    document.querySelectorAll('.section-slider-news').forEach(section => {

        new Flickity(section.querySelector('.articles'), {
            prevNextButtons: true,
            pageDots: false,
            cellAlign: 'left',
            contain: true,
            on: {
                ready: () => window.dispatchEvent(new CustomEvent('resize'))
            }
        });
    })
};
