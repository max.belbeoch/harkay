const Flickity = require('flickity');

export const initMissionImages = () => {
    const container = document.querySelector('.page-mission .list-images');
    if(container && container.querySelectorAll('.img').length > 1) {
        new Flickity(container, {
            prevNextButtons: false,
            pageDots: true,
            autoPlay: true,
            on: {
                ready:() => window.dispatchEvent(new CustomEvent('resize'))
            }
        })
    }
};
