import Plyr from 'plyr';

export const initVideoPlayer = () => {
    document.querySelectorAll('.video-player').forEach(el => {
        new Plyr(el);
    })
};
