export const initMap = () => {

    const section = document.querySelector('.section-map');
    const countries = section ? section.dataset.country.split('#') : [];

    if (countries.length) {

        const svg = section.querySelector(".map svg");

        countries.forEach(country => {
            let paths = svg.querySelectorAll(`.${country}`);
            let tooltip = section.querySelector(`.tooltip[data-country='${country}']`);
            let isHover = false;

            if (paths && paths.length) {
                if(tooltip) {
                    document.addEventListener('mousemove', e => {
                        if(isHover) {
                            tooltip.style.left = e.clientX + 'px';
                            tooltip.style.top = e.clientY + 'px';
                        }
                    }, false);
                }

                paths.forEach(path => {
                    path.classList.add('active');

                    path.addEventListener('mouseenter', () => {
                        isHover = true;
                        paths.forEach(p => p.classList.add('is-hover'));
                        tooltip.classList.add('visible');
                    });

                    path.addEventListener('mouseleave', () => {
                        isHover = false;
                        paths.forEach(p => p.classList.remove('is-hover'));
                        tooltip.classList.remove('visible');
                    });

                    path.addEventListener('click', () => {
                        window.location = tooltip.dataset.link;
                    })
                })
            }
        })
    }
};

