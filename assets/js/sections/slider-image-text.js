const Flickity = require('flickity');

export const initSliderImageText = () => {


    document.querySelectorAll('.section-slider-image-text').forEach(section => {

        const imageSlider = new Flickity(section.querySelector('.images'), {
            draggable: false,
            prevNextButtons: false,
            pageDots: false
        });
        const textSlider = new Flickity(section.querySelector('.texts'), {
            prevNextButtons: false,
            pageDots: true,
            on: {
                ready: () => window.dispatchEvent(new CustomEvent('resize'))
            }
        });

        textSlider.on('change', (e) => {
            imageSlider.select(e)
        })
    })
};
