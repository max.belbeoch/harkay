export const initStickyHeader = () => {
    const header = document.querySelector('.header');
    if(header) {
        window.addEventListener('scroll', () => {
            if(window.scrollY > 100) {
                header.classList.add('sticky')
            } else {
                header.classList.remove('sticky')
            }
        })
    }
};
