const Flickity = require('flickity');

export const initSliderMissions = () => {
    document.querySelectorAll('.section-slider-missions .slider').forEach(slider => {
        new Flickity(slider, {
            prevNextButtons: true,
            pageDots: false,
            freeScroll: true,
            touchVerticalScroll: true,
            on: {
                ready:() => window.dispatchEvent(new CustomEvent('resize'))
            }
        })
    })
};
