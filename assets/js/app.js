import { initLanguageSwitcher } from "./sections/lang-switcher";

require('../styles/app.scss');

import {initMissionImages} from "./sections/mission-images";
import {initStickyHeader} from "./sections/sticky-header";
import {initMobileMenu} from "./sections/mobile-menu";
import {initSliderImageText} from "./sections/slider-image-text";
import {initSliderMissions} from "./sections/slider-missions";
import {initMap} from "./sections/map";
import {initCountryInfos} from "./sections/country";
import {initSliderNews} from "./sections/slider-news";
import {initVideoPlayer} from "./sections/video";
import { initAdmin } from "./pages/admin";
import { initNewsletter } from "./sections/newsletter";
import { initLazyLoading } from "./helpers/lazy-loading";

document.addEventListener("DOMContentLoaded", () => {

    initLazyLoading();
    initStickyHeader();
    initSliderImageText();
    initSliderMissions();
    initMap();
    initCountryInfos();
    initSliderNews();
    initMobileMenu();
    initVideoPlayer();
    initMissionImages();
    initAdmin();
    initNewsletter();
    initLanguageSwitcher();

});

