const initLanguagesTabs = () => {
    document.querySelectorAll('.lang-tabs').forEach(parent => {
        const tabs = parent.querySelectorAll('.tab');
        const links = parent.querySelectorAll('li');
        links.forEach(link => {
            link.addEventListener('click', () => {
                tabs.forEach(tab => tab.classList.remove('active'));
                links.forEach(li => li.classList.remove('active'));
                link.classList.add('active');
                tabs[[...links].indexOf(link)].classList.add('active')
            })
        })
    })
};

const displayInfos = (input, text, max, min) => {
    input.maxLength = max;
    input.minLength = min;
    const label = input.closest('div').querySelector('label');
    const infos = document.createElement('span');
    infos.classList.add('infos');
    infos.innerText = text;
    label.append(infos);
    const count = document.createElement('span');
    count.classList.add('count');
    infos.append(count);
    input.addEventListener('keydown', () => {
        setTimeout(() => {
            const length = input.value.length;
            count.className = length > max || length < min ? 'count error' : 'count';
            count.innerText = `${length}/${max}`
        })
    })
};

export const initAdmin = () => {

    initLanguagesTabs();

    ['country', 'post', 'mission', 'general'].forEach(name => {
        ['fr', 'es', 'en'].forEach(lang => {

            // Display letter count
            const inputs = {
                short_content: document.querySelector(`#${name}_form_short_content_${lang}`),
                seo: {
                    title: document.querySelector(`#${name}_form_page_title_${lang}`),
                    description: document.querySelector(`#${name}_form_page_description_${lang}`),
                }
            };

            if (inputs.seo.title) {
                displayInfos(inputs.seo.title, 'Le titre doit faire entre 30 et 60 caractères', 60, 30);
            }
            if (inputs.seo.description) {
                displayInfos(inputs.seo.description, 'La description doit faire entre 50 et 160 caractères', 160, 50);
            }
            if (inputs.short_content) {
                displayInfos(inputs.short_content, '1000 caractères maximum', 1000, 0)
            }

        })
    })
};

