export const initLazyLoading = () => {

    const imageObserver = new IntersectionObserver(entries => {
        entries.forEach((entry) => {
            if (entry.isIntersecting) {

                const img = entry.target;
                img.classList.add('lazy-img');

                img.src = img.dataset.src;

                img.addEventListener('load', () => {
                    img.classList.add('loaded')
                });

                imageObserver.unobserve(img)
            }
        })
    });

    imageObserver.POLL_INTERVAL = 500;

    document.querySelectorAll('img[data-src]').forEach(img => imageObserver.observe(img))
};
