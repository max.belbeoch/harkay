# Harkay
- Quetin Huet
- Maëva Pasteur
- Maxime Beldeoch

<br> <br>

***

## Installation

``composer install``

#### Mettre à jour le .env 

``DATABASE_URL=mysql://username:password@127.0.0.1:PORT/NOM_DE_LA_BD?serverVersion=5.7``

#### Créer la base 

``symfony console doctrine:database:create``

#### Créer les tables 

``symfony console doctrine:migrations:migrate``

### Créer un user admin a la main 

Mettre ``["ROLE_ADMIN"]`` dans le champ roles. <br>
Pour le password générer un mdp avec la commande suivante et l'enregistrer en base :

```symfony console security:encode-password```

#### Lancer l'app depuis le dossier site 

``symfony serve``

<br> <br>

***

## Lancer webpack

```
composer update 
yarn install
yarn watch
```
