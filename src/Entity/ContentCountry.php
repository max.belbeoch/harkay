<?php

namespace App\Entity;

use App\Repository\ContentCountryRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ContentCountryRepository::class)
 */
class ContentCountry
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $lang;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $country_id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $page_title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $page_description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=1024, nullable=true)
     */
    private $short_content;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $content_1;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $content_2;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $content_3;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $published;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $last_updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deleted_at;

    public function __construct()
    {
        $this->setPublished(false);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLang(): ?string
    {
        return $this->lang;
    }

    public function setLang(string $lang): self
    {
        $this->lang = $lang;

        return $this;
    }

    public function getCountryId(): ?string
    {
        return $this->country_id;
    }

    public function setCountryId(string $country_id): self
    {
        $this->country_id = $country_id;

        return $this;
    }

    public function getPageTitle(): ?string
    {
        return $this->page_title;
    }

    public function setPageTitle(string $page_title): self
    {
        $this->page_title = $page_title;

        return $this;
    }

    public function getPageDescription(): ?string
    {
        return $this->page_description;
    }

    public function setPageDescription(string $page_description): self
    {
        $this->page_description = $page_description;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getShortContent(): ?string
    {
        return $this->short_content;
    }

    public function setShortContent(string $short_content): self
    {
        $this->short_content = $short_content;

        return $this;
    }

    public function getContent1(): ?string
    {
        return $this->content_1;
    }

    public function setContent1(string $content): self
    {
        $this->content_1 = $content;

        return $this;
    }

    public function getContent2(): ?string
    {
        return $this->content_2;
    }

    public function setContent2(string $content): self
    {
        $this->content_2 = $content;

        return $this;
    }

    public function getContent3(): ?string
    {
        return $this->content_3;
    }

    public function setContent3(string $content): self
    {
        $this->content_3 = $content;

        return $this;
    }

    public function getPublished(): ?bool
    {
        return $this->published;
    }

    public function setPublished(bool $published): self
    {
        $this->published = $published;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getLastUpdatedAt(): ?\DateTimeInterface
    {
        return $this->last_updated_at;
    }

    public function setLastUpdatedAt(\DateTimeInterface $last_updated_at): self
    {
        $this->last_updated_at = $last_updated_at;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deleted_at;
    }

    public function setDeletedAt(\DateTimeInterface $deleted_at): self
    {
        $this->deleted_at = $deleted_at;

        return $this;
    }
}
