<?php

namespace App\Entity;

use App\Repository\GeneralRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GeneralRepository::class)
 */
class General
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $page_title_fr;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $page_description_fr;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $page_title_en;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $page_description_en;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $page_title_es;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $page_description_es;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $legal_name;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $siret;

    /**
     * @ORM\Column(type="string", length=125, nullable=true)
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=125, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="integer", length=10, nullable=true)
     */
    private $zip_code;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $phone_number;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $donation_link;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $fb_link;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $twt_link;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $ig_link;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $yt_link;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $last_updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deleted_at;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPageTitleFr(): ?string
    {
        return $this->page_title_fr;
    }

    public function setPageTitleFr($page_title_fr): self
    {
        $this->page_title_fr = $page_title_fr;

        return $this;
    }

    public function getPageDescriptionFr(): ?string
    {
        return $this->page_description_fr;
    }

    public function setPageDescriptionFr($page_description_fr): self
    {
        $this->page_description_fr = $page_description_fr;

        return $this;
    }

    public function getPageTitleEn(): ?string
    {
        return $this->page_title_en;
    }

    public function setPageTitleEn($page_title_en): self
    {
        $this->page_title_en = $page_title_en;

        return $this;
    }

    public function getPageDescriptionEn(): ?string
    {
        return $this->page_description_en;
    }

    public function setPageDescriptionEn($page_description_en): self
    {
        $this->page_description_en = $page_description_en;

        return $this;
    }

    public function getPageTitleEs(): ?string
    {
        return $this->page_title_es;
    }

    public function setPageTitleEs($page_title_es): self
    {
        $this->page_title_es = $page_title_es;

        return $this;
    }

    public function getPageDescriptionEs(): ?string
    {
        return $this->page_description_es;
    }

    public function setPageDescriptionEs($page_description_es): self
    {
        $this->page_description_es = $page_description_es;

        return $this;
    }

    public function getLegalName(): ?string
    {
        return $this->legal_name;
    }

    public function setLegalName($legal_name): self
    {
        $this->legal_name = $legal_name;

        return $this;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret($siret): self
    {
        $this->siret = $siret;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry($country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity($city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getZipCode(): ?int
    {
        return $this->zip_code;
    }

    public function setZipCode($zip_code): self
    {
        $this->zip_code = $zip_code;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress($address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail($email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phone_number;
    }

    public function setPhoneNumber($phone_number): self
    {
        $this->phone_number = $phone_number;

        return $this;
    }

    public function getDonationLink(): ?string
    {
        return $this->donation_link;
    }

    public function setDonationLink($donation_link): self
    {
        $this->donation_link = $donation_link;

        return $this;
    }

    public function getFbLink(): ?string
    {
        return $this->fb_link;
    }

    public function setFbLink($fb_link): self
    {
        $this->fb_link = $fb_link;

        return $this;
    }

    public function getTwtLink(): ?string
    {
        return $this->twt_link;
    }

    public function setTwtLink($twt_link): self
    {
        $this->twt_link = $twt_link;

        return $this;
    }

    public function getIgLink(): ?string
    {
        return $this->ig_link;
    }

    public function setIgLink($ig_link): self
    {
        $this->ig_link = $ig_link;

        return $this;
    }

    public function getYtLink(): ?string
    {
        return $this->yt_link;
    }

    public function setYtLink($yt_link): self
    {
        $this->yt_link = $yt_link;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getLastUpdatedAt(): ?\DateTimeInterface
    {
        return $this->last_updated_at;
    }

    public function setLastUpdatedAt(\DateTimeInterface $last_updated_at): self
    {
        $this->last_updated_at = $last_updated_at;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deleted_at;
    }

    public function setDeletedAt(\DateTimeInterface $deleted_at): self
    {
        $this->deleted_at = $deleted_at;

        return $this;
    }
}
