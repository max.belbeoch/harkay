<?php

namespace App\Entity;

use App\Repository\CountryRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CountryRepository::class)
 */
class Country
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $author;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $page_display_newsletter;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $page_display_post;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $page_display_mission;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $page_display_map;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $last_updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deleted_at;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAuthor(): ?int
    {
        return $this->author;
    }

    public function setAuthor(int $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getPageDisplayNewsletter(): ?bool
    {
        return $this->page_display_newsletter;
    }

    public function setPageDisplayNewsletter(bool $page_display_newsletter): self
    {
        $this->page_display_newsletter = $page_display_newsletter;

        return $this;
    }

    public function getPageDisplayPost(): ?bool
    {
        return $this->page_display_post;
    }

    public function setPageDisplayPost(bool $page_display_post): self
    {
        $this->page_display_post = $page_display_post;

        return $this;
    }

    public function getPageDisplayMission(): ?bool
    {
        return $this->page_display_mission;
    }

    public function setPageDisplayMission(bool $page_display_mission): self
    {
        $this->page_display_mission = $page_display_mission;

        return $this;
    }

    public function getPageDisplayMap(): ?bool
    {
        return $this->page_display_map;
    }

    public function setPageDisplayMap(bool $page_display_map): self
    {
        $this->page_display_map = $page_display_map;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getLastUpdatedAt(): ?\DateTimeInterface
    {
        return $this->last_updated_at;
    }

    public function setLastUpdatedAt(\DateTimeInterface $last_updated_at): self
    {
        $this->last_updated_at = $last_updated_at;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deleted_at;
    }

    public function setDeletedAt(\DateTimeInterface $deleted_at): self
    {
        $this->deleted_at = $deleted_at;

        return $this;
    }
}
