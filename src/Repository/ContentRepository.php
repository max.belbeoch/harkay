<?php

namespace App\Repository;

use App\Entity\Content;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Content|null find($id, $lockMode = null, $lockVersion = null)
 * @method Content|null findOneBy(array $criteria, array $orderBy = null)
 * @method Content[]    findAll()
 * @method Content[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Content::class);
    }

    public function findAll($params = [], $lang = 'FR')
    {

        if($lang){
            $params['lang'] = strtoupper($lang);
        }

        return $this->findBy(
            $params, 
            ['id' => 'DESC']);
    }

    public function findByPostId($post_id, $lang = 'FR')
    {
        return $this->findBy(
            [
                'post_id' => $post_id,
                'lang' => strtoupper($lang)
            ],
            [
                'id' => 'DESC'
            ]
        );
    }

    public function findByMissionId($mission_id, $lang = 'FR')
    {
        return $this->findBy(
            [
                'mission_id' => $mission_id,
                'lang' => strtoupper($lang)
            ],
            [
                'id' => 'DESC'
            ]
        );
    }

}
