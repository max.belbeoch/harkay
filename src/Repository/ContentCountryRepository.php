<?php

namespace App\Repository;

use App\Entity\ContentCountry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ContentCountry|null find($id, $lockMode = null, $lockVersion = null)
 * @method ContentCountry|null findOneBy(array $criteria, array $orderBy = null)
 * @method ContentCountry[]    findAll()
 * @method ContentCountry[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContentCountryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ContentCountry::class);
    }

    public function findAll($params = [], $lang = 'FR')
    {

        if($lang){
            $params['lang'] = strtoupper($lang);
        }

        return $this->findBy(
            $params, 
            ['id' => 'DESC']);
    }
    
}
