<?php

namespace App\Controller;

use App\Entity\Content;
use App\Entity\ContentCountry;
use App\Entity\Country;
use App\Entity\Mission;
use App\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SitemapController extends AbstractController
{
    /**
     * @Route("/sitemap.xml", name="sitemap", defaults={"_format"="xml"})
     */
    public function index(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $lang = $request->getLocale();

        // Nous récupérons le nom d'hôte depuis l'URL
        $hostname = $request->getSchemeAndHttpHost();

        // On initialise un tableau pour lister les URLs
        $urls = [];

        // On ajoute les URLs "statiques"
        $urls[] = ['loc' => $this->generateUrl('home')];
        $urls[] = ['loc' => $this->generateUrl('missions')];
        $urls[] = ['loc' => $this->generateUrl('posts')];
        $urls[] = ['loc' => $this->generateUrl('countries')];

        // On récupère les URLS de chaque article
        $posts = $entityManager->getRepository(Post::class)->findAll();
        if (isset($posts) && !empty($posts)) {
            foreach ($posts as &$post) {
                $post->content = $entityManager->getRepository(Content::class)->findOneBy(['post_id' => $post->getId(), 'lang' => $lang], ['id' => 'DESC']);
                $urls[] = [
                    'loc' => $this->generateUrl('post', ['id' => $post->getId()]),
                    'lastmod' => $post->getLastUpdatedAt()->format('Y-m-d'),
                    'image' => [
                        'loc' => "/uploads/img_article/" . $post->getImage(),
                        'title' => $post->content->getTitle()
                    ]
                ];
            }
        }

        // On récupère les URLS de chaque mission
        $missions = $entityManager->getRepository(Mission::class)->findAll();
        if (isset($missions) && !empty($missions)) {
            foreach ($missions as &$mission) {
                $mission->content = $entityManager->getRepository(Content::class)->findOneBy(['mission_id' => $mission->getId(), 'lang' => $lang]);
                $urls[] = [
                    'loc' => $this->generateUrl('mission', ['id' => $mission->getId()]),
                    'lastmod' => $mission->getLastUpdatedAt()->format('Y-m-d'),
                    'image' => [
                        'loc' => "/uploads/img_article/" . $mission->getImage(),
                        'title' => $mission->content->getTitle()
                    ]
                ];
            };
        }

        // On récupère les URLS de chaque pays
        $countries = $entityManager->getRepository(Country::class)->findAll();
        if (isset($countries) && !empty($countries)) {
            foreach ($countries as &$country) {
                $country->content = $entityManager->getRepository(ContentCountry::class)->findOneBy(['country_id' => $country->getId(), 'lang' => $lang], ['id' => 'DESC']);
                $urls[] = [
                    'loc' => $this->generateUrl('country', ['id' => $country->getId()]),
                    'lastmod' => $country->getLastUpdatedAt()->format('Y-m-d'),
                    'image' => [
                        'loc' => "/uploads/img_article/" . $country->getImage(),
                        'title' => $country->content->getTitle()
                    ]
                ];
            };
        }

        // Fabrication de la réponse XML
        $response = new Response(
            $this->renderView('sitemap/index.html.twig', [
                'urls' => $urls,
                'hostname' => $hostname
            ]),
            200
        );

        // Ajout des entêtes
        $response->headers->set('Content-Type', 'text/xml');

        // On envoie la réponse
        return $response;
    }
}
