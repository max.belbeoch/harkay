<?php

namespace App\Controller;

use App\Entity\Mission;
use App\Entity\Content;
use DateTime;
use DateTimeZone;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\MissionFormType;
use App\Service\ContentService;
use App\Service\FileService;


class AdminMissionController extends AbstractController
{

    /**
     * @Route("/admin/mission/ajouter", name="admin_add_mission")
     */
    public function addMission(Request $request, FileService $fileService, ContentService $contentService): Response
    {
        $mission = new Mission();

        $form = $this->createForm(MissionFormType::class, $mission);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $image = $form->get('image')->getData();

            $mission = $form->getData();

            if ($image) {
                $newFileName = $fileService->uploadImage($image);
                $mission->setImage($newFileName);
            }

            $mission->setAuthor($this->getUser()->getId());
            $mission->setCreatedAt(new DateTime("now", new DateTimeZone('GMT+2')));
            $mission->setLastUpdatedAt(new DateTime("now", new DateTimeZone('GMT+2')));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($mission);
            $entityManager->flush();

            foreach (Content::LANG as $key => $lang) {
                $content = new Content();
                $content->setLang($key);
                $content->setMissionId($mission->getId());
                $contentService->setContent($content, $form, true);
                $entityManager->persist($content);
            }

            $entityManager->flush();

            return $this->redirectToRoute('admin_missions');
        }

        return $this->render('admin/add_mission.html.twig', [
            'form_mission' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/mission/modifier/{id}", name="admin_update_mission")
     */
    public function updateMission(Request $request, FileService $fileService, ContentService $contentService, $id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $mission = $entityManager->getRepository(Mission::class)->findOneBy(['id' => $id]);
        $contents = $entityManager->getRepository(Content::class)->findAll(['mission_id' => $mission->getId()], false);
        $error = false;

        if ($mission != null) {

            $form = $this->createForm(MissionFormType::class, $mission);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $image = $form->get('image')->getData();
                $mission = $form->getData();

                if ($image) {
                    $newFileName = $fileService->uploadImage($image);
                    $mission->setImage($newFileName);
                }

                $mission->setLastUpdatedAt(new DateTime("now", new DateTimeZone('GMT+2')));

                foreach ($contents as $content) {
                    $contentService->setContent($content, $form);
                    $entityManager->persist($content);
                }

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($mission);
                $entityManager->flush();

                return $this->redirectToRoute('admin_missions');
            }

            if (!$form->isSubmitted()) {
                $contentService->setFormContent($form, $contents);
            }
        } else {
            $error = 'Mission inconnue';
        }

        return $this->render('admin/update_mission.html.twig', [
            'form_mission' => $form->createView(),
            'error' => $error
        ]);
    }
}
