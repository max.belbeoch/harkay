<?php

namespace App\Controller;

use App\Entity\Content;
use App\Entity\Country;
use App\Entity\ContentCountry;
use DateTime;
use DateTimeZone;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\CountryFormType;
use App\Service\ContentService;
use App\Service\FileService;

class AdminCountryController extends AbstractController
{

    /**
     * @Route("/admin/pays/ajouter", name="admin_add_country")
     */
    public function addCountry(Request $request, FileService $fileService, ContentService $contentService): Response
    {
        $country = new Country();
        $error = false;

        $form = $this->createForm(CountryFormType::class, $country);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();

            $exists = $entityManager->getRepository(Country::class)->findOneBy(['country' => $form->get('country')->getData()]);

            if ($exists == null) {

                $image = $form->get('image')->getData();

                $country = $form->getData();

                if ($image) {
                    $newFileName = $fileService->uploadImage($image);
                    $country->setImage($newFileName);
                }

                $country->setAuthor($this->getUser()->getId());
                $country->setCreatedAt(new DateTime("now", new DateTimeZone('GMT+2')));
                $country->setLastUpdatedAt(new DateTime("now", new DateTimeZone('GMT+2')));

                $entityManager->persist($country);
                $entityManager->flush();

                foreach (Content::LANG as $key => $lang) {
                    $content = new ContentCountry();
                    $content->setLang($key);
                    $content->setCountryId($country->getId());
                    $contentService->setCountryContent($content, $form, true);
                    $entityManager->persist($content);
                }

                $entityManager->flush();

                return $this->redirectToRoute('admin_countries');
            } else {
                $error = 'Création impossible : le pays existe déjà';
            }
        }

        return $this->render('admin/add_country.html.twig', [
            'form_country' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/admin/country/modifier/{id}", name="admin_update_country")
     */
    public function updatecountry(Request $request, FileService $fileService, ContentService $contentService, $id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $country = $entityManager->getRepository(Country::class)->findOneBy(['id' => $id]);
        $contents = $entityManager->getRepository(ContentCountry::class)->findAll(['country_id' => $country->getId()], false);
        $error = false;

        if ($country != null) {

            $form = $this->createForm(CountryFormType::class, $country);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $image = $form->get('image')->getData();

                $countryForm = $form->getData();

                if ($image) {
                    $newFileName = $fileService->uploadImage($image);
                    $countryForm->setImage($newFileName);
                }

                $countryForm->setLastUpdatedAt(new DateTime("now", new DateTimeZone('GMT+2')));

                foreach ($contents as $content) {
                    $contentService->setCountryContent($content, $form);
                    $entityManager->persist($content);
                }

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($countryForm);
                $entityManager->flush();

                return $this->redirectToRoute('admin_countries');
            }
        } else {
            $error = 'Pays inconnu';
        }

        if (!$form->isSubmitted()) {
            $contentService->setFormCountryContent($form, $contents);
        }

        return $this->render('admin/update_country.html.twig', [
            'error' => $error,
            'form_country' => $form->createView(),
        ]);
    }
}
