<?php

namespace App\Controller;

use App\Entity\User;
use DateTime;
use DateTimeZone;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Form\UserFormType;

class AdminUserController extends AbstractController
{

/**
     * @Route("/admin/utilisateurs", name="admin_users")
     */
    public function indexUsers(Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $users = $entityManager->getRepository(User::class)->findAll();

        return $this->render('admin/users.html.twig', [
            'users' =>  $users,
        ]);
    }

    /**
     * @Route("/admin/utilisateurs/ajouter", name="admin_add_user")
     */
    public function addUser(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {

        $user = new User();

        $form = $this->createForm(UserFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user = $form->getData();

            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('password')->getData()
                )
            );

            $user->setRoles(['ROLE_ADMIN']);
            $user->setCreatedAt(new DateTime("now", new DateTimeZone('GMT+2')));
            $user->setLastUpdatedAt(new DateTime("now", new DateTimeZone('GMT+2')));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('admin_users');
        }

        return $this->render('admin/add_user.html.twig', [
            'form_user' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/utilisateurs/supprimer/{id}", name="admin_delete_user")
     */
    public function deleteUser(Request $request): Response
    {
        return $this->render('admin/users.html.twig');
    }

}