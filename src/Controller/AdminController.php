<?php

namespace App\Controller;

use App\Entity\Content;
use App\Entity\Country;
use App\Entity\ContentCountry;
use App\Entity\General;
use App\Entity\Post;
use App\Entity\Mission;
use App\Form\GeneralFormType;
use App\Service\ContentService;
use App\Service\CountryService;
use DateTime;
use DateTimeZone;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{

    private $contentService;

    public function __construct(ContentService $contentService)
    {
        $this->contentService = $contentService;
    }

    /**
     * @Route("/admin", name="admin_home",requirements={ "_locale": "en|fr|es",})
     */
    public function index(Request $request): Response
    {
        $create = false;
        $entityManager = $this->getDoctrine()->getManager();

        $general = $entityManager->getRepository(General::class)->findOneBy(['id' => 1]);

        if ($general == null) {
            $create = true;
            $general = new General();
        }

        $form = $this->createForm(GeneralFormType::class, $general);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $general = $form->getData();

            if ($create) {
                $general->setCreatedAt(new DateTime("now", new DateTimeZone('GMT+2')));
            }

            $general->setLastUpdatedAt(new DateTime("now", new DateTimeZone('GMT+2')));

            $entityManager->persist($general);
            $entityManager->flush();

            return $this->redirectToRoute('admin_home');
        }

        return $this->render('admin/index.html.twig', [
            'form_general' => $form->createView(),
            'general' => $this->contentService->getGeneral($entityManager, $request->getLocale())
        ]);

    }

    /**
     * @Route("/admin/articles", name="admin_posts")
     */
    public function posts(): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $posts = $entityManager->getRepository(Post::class)->findAll();

        foreach ($posts as &$post) {
            $post->content = $entityManager->getRepository(Content::class)->findOneBy(['post_id' => $post->getId(), 'lang' => 'FR']);
        }

        return $this->render('admin/articles.html.twig', [
            'posts' =>  $posts
        ]);
    }

    /**
     * @Route("/admin/missions", name="admin_missions")
     */
    public function missions(CountryService $countryService): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $missions = $entityManager->getRepository(Mission::class)->findAll();

        // set to display label
        if (isset($missions) && !empty($missions)) {
            foreach ($missions as &$mission) {
                $mission->setCountry($countryService->getCountryLabelToDisplay($mission->getCountry()));
                $mission->content = $entityManager->getRepository(Content::class)->findOneBy(['mission_id' => $mission->getId(), 'lang' => 'FR']);
            }
        }

        return $this->render('admin/missions.html.twig', [
            'missions' => $missions
        ]);
    }

    /**
     * @Route("/admin/pays", name="admin_countries")
     */
    public function countries(CountryService $countryService): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $countries = $entityManager->getRepository(Country::class)->findAll();

        // set to display label
        if (isset($countries) && !empty($countries)) {
            foreach ($countries as &$country) {
                $country->countryLabel = $countryService->getCountryLabelToDisplay($country->getCountry());
                $country->content = $entityManager->getRepository(ContentCountry::class)->findOneBy(['country_id' => $country->getId(), 'lang' => 'FR']);
            }
        }

        return $this->render('admin/countries.html.twig', [
            'countries' => $countries
        ]);
    }
}
