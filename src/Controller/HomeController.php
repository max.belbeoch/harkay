<?php

namespace App\Controller;

use App\Entity\General;
use App\Entity\Mission;
use App\Entity\Post;
use App\Entity\User;
use App\Entity\Content;
use App\Entity\ContentCountry;
use App\Entity\Country;
use App\Service\ContentService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\CountryService;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;

class HomeController extends AbstractController
{
    private $countryService;
    private $contentService;

    public function __construct(CountryService $countryService, ContentService $contentService)
    {
        $this->countryService = $countryService;
        $this->contentService = $contentService;
    }

    /**
     * @Route("/{_locale?fr}", name="home", requirements={"_locale"="^[A-Za-z]{2}$"})
     */
    public function index(Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $error = [
            'posts' => false,
            'missions' => false,
            'countries' => false
        ];

        // Getting posts
        $posts = $entityManager->getRepository(Post::class)->findBy([], ['id' => 'DESC'], 5);
        if (isset($posts) && !empty($posts)) {
            foreach ($posts as &$post) {
                $post->content = $entityManager->getRepository(Content::class)->findOneBy(['post_id' => $post->getId(), 'lang' => $request->getLocale()], ['id' => 'DESC']);
            }
        } else {
            $error['posts'] = 'Aucun article ...';
        }

        // Getting missions
        $missions = $entityManager->getRepository(Mission::class)->findBy([], ['id' => 'DESC'], 5);
        // set to display label
        if (isset($missions) && !empty($missions)) {
            foreach ($missions as &$mission) {
                $mission->setCountry($this->countryService->getCountryLabelToDisplay($mission->getCountry()));
                $mission->content = $entityManager->getRepository(Content::class)->findOneBy(['mission_id' => $mission->getId(), 'lang' => $request->getLocale()]);
            }
        } else {
            $error['missions'] = 'Aucune mission ...';
        }

        // Getting countries
        $countries = $entityManager->getRepository(Country::class)->findBy([], ['id' => 'DESC']);
        $tabCountries = [];

        if (isset($countries) && !empty($countries)) {
            foreach ($countries as &$country) {
                $country->content = $entityManager->getRepository(ContentCountry::class)->findOneBy(['country_id' => $country->getId(), 'lang' => $request->getLocale()], ['id' => 'DESC']);
                $nbMissions = $entityManager->getRepository(Mission::class)->findBy(['country' => $country->getCountry()]);
                $tabCountries[$country->getCountry()] = [
                    'name' => $this->countryService->getCountryLabelToDisplay($country->getCountry()),
                    "image" => "/uploads/img_article/" . $country->getImage(),
                    'link' => $this->generateUrl('country', ['id' => $country->getId()]),
                    'missions' => sizeof($nbMissions),
                    'content' => $country->content->getShortContent()
                ];
            }
        } else {
            $error['countries'] = 'Aucun pays ...';
        }

        return $this->render('pages/home.html.twig', [
            'error' => $error,
            'posts' => $posts,
            'missions' => $missions,
            'countries' => $tabCountries,
            'general' => $this->contentService->getGeneral($entityManager, $request->getLocale())
        ]);
    }

    /**
     * @Route("{_locale}/articles", name="posts", requirements={"_locale"="^[A-Za-z]{2}$"})
     */
    public function posts(Request $request, PaginatorInterface $paginator): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $posts = $entityManager->getRepository(Post::class)->findAll();
        foreach ($posts as &$post) {
            $post->content = $entityManager->getRepository(Content::class)->findOneBy(['post_id' => $post->getId(), 'lang' => $request->getLocale()]);
        }

        // Add pagination
        $active_page = $request->query->get('page');
        if (!$active_page) {
            $active_page = 1;
        }
        $posts = $paginator->paginate(
            $posts,
            $request->query->getInt('page', $active_page),
            9
        );

        return $this->render('pages/articles.html.twig', [
            'posts' => $posts,
            'general' => $this->contentService->getGeneral($entityManager, $request->getLocale())
        ]);
    }

    /**
     * @Route("{_locale}/article/{id}", name="post", requirements={"_locale"="^[A-Za-z]{2}$"})
     */
    public function post(Request $request, $id): Response
    {
        $lang = $request->getLocale();
        $entityManager = $this->getDoctrine()->getManager();
        $post = $entityManager->getRepository(Post::class)->findOneBy(['id' => $id]);
        $post->content = $entityManager->getRepository(Content::class)->findOneBy(['post_id' => $id, 'lang' => $lang]);
        $author = $entityManager->getRepository(User::class)->findOneBy(['id' => $post->getAuthor()]);

        // Get articles
        $posts = $entityManager->getRepository(Post::class)->findBy([], ['id' => 'DESC'], 5);
        foreach ($posts as $key => &$post_item) {
            $post_id =  $post_item->getId();
            // Remove main article from array
            if ($post_id == $id) {
                unset($posts[$key]);
            } else {
                $post_item->content = $entityManager->getRepository(Content::class)->findOneBy(['post_id' => $post_id, 'lang' => $lang], ['id' => 'DESC']);
            }
        }
        // Limit to 4 articles
        if (count($posts) == 5) {
            unset($posts[4]);
        }

        return $this->render('pages/article.html.twig', [
            'post' => $post,
            'author' => $author,
            'posts' => $posts,
            'lang' => $lang,
            'general' => $this->contentService->getGeneral($entityManager, $request->getLocale())
        ]);
    }

    /**
     * @Route("{_locale}/missions", name="missions", requirements={"_locale"="^[A-Za-z]{2}$"})
     */
    public function missions(CountryService $countryService, Request $request, PaginatorInterface $paginator): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $missions = $entityManager->getRepository(Mission::class)->findAll();

        // set to display label
        if (isset($missions) && !empty($missions)) {
            foreach ($missions as &$mission) {
                $mission->setCountry($countryService->getCountryLabelToDisplay($mission->getCountry()));
                $mission->content = $entityManager->getRepository(Content::class)->findOneBy(['mission_id' => $mission->getId(), 'lang' => $request->getLocale()]);
            }
        }

        // Add pagination
        $active_page = $request->query->get('page');
        if (!$active_page) {
            $active_page = 1;
        }
        $missions = $paginator->paginate(
            $missions,
            $request->query->getInt('page', $active_page),
            9
        );

        return $this->render('pages/missions.html.twig', [
            'missions' => $missions,
            'general' => $this->contentService->getGeneral($entityManager, $request->getLocale())
        ]);
    }

    /**
     * @Route("{_locale}/mission/{id}", name="mission", requirements={"_locale"="^[A-Za-z]{2}$"})
     */
    public function mission(Request $request, $id, CountryService $countryService): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $mission = $entityManager->getRepository(Mission::class)->findOneBy(['id' => $id]);
        $mission->content = $entityManager->getRepository(Content::class)->findOneBy(['mission_id' => $mission->getId(), 'lang' => $request->getLocale()]);
        $missions = $entityManager->getRepository(Mission::class)->findAll();

        // set to display label
        if (isset($missions) && !empty($missions)) {
            foreach ($missions as &$mission_item) {
                $mission_item->setCountry($countryService->getCountryLabelToDisplay($mission_item->getCountry()));
                $mission_item->content = $entityManager->getRepository(Content::class)->findOneBy(['mission_id' => $mission->getId(), 'lang' => $request->getLocale()]);
            }
        }

        return $this->render('pages/mission.html.twig', [
            'mission' => $mission,
            'missions' => $missions,
            'general' => $this->contentService->getGeneral($entityManager, $request->getLocale())
        ]);
    }

    /**
     * @Route("{_locale}/pays", name="countries")
     */
    public function countries(CountryService $countryService, Request $request)
    {

        $entityManager = $this->getDoctrine()->getManager();

        $countries = $entityManager->getRepository(Country::class)->findBy([], ['id' => 'DESC']);
        $tabCountries = [];

        foreach ($countries as &$country) {
            $country->content = $entityManager->getRepository(ContentCountry::class)->findOneBy(['country_id' => $country->getId(), 'lang' => $request->getLocale()], ['id' => 'DESC']);
            $nbMissions = $entityManager->getRepository(Mission::class)->findBy(['country' => $country->getCountry()]);
            $tabCountries[$country->getCountry()] = [
                'name' => $countryService->getCountryLabelToDisplay($country->getCountry()),
                "image" => "/uploads/img_article/" . $country->getImage(),
                'link' => 'pays/' . $country->getId(),
                'missions' => sizeof($nbMissions),
                'content' => $country->content->getShortContent()
            ];
        }

        return $this->render('pages/pays.html.twig', [
            'countries' => $tabCountries,
            'general' => $this->contentService->getGeneral($entityManager, $request->getLocale())
        ]);
    }

    /**
     * @Route("{_locale}/pays/{id}", name="country")
     */
    public function country(Request $request, CountryService $countryService, $id): Response
    {
        $lang = $request->getLocale();
        $entityManager = $this->getDoctrine()->getManager();

        $country = $entityManager->getRepository(Country::class)->findOneBy(['id' => $id], ['id' => 'DESC']);

        // Getting country
        if (isset($country) && !empty($country)) {

            $blocks = $this->getPageBlock($request->getLocale(), $country);

            $country = $entityManager->getRepository(Country::class)->findOneBy(['id' => $id], ['id' => 'DESC']);
            $country->content = $entityManager->getRepository(ContentCountry::class)->findOneBy(['country_id' => $country->getId(), 'lang' => $lang]);
            $country->title = $countryService->getCountryLabelToDisplay($country->getCountry());
            $country->description = $country->content->getShortContent();
            $country->peoples = $country->content->getContent1();
            $country->culture = $country->content->getContent2();
            $country->protect = $country->content->getContent3();

            // Get missions in this country
            $missions = $entityManager->getRepository(Mission::class)->findBy(['country' => $country->getCountry()]);
            if (isset($missions) && !empty($missions)) {
                foreach ($missions as &$mission) {
                    $mission->setCountry($countryService->getCountryLabelToDisplay($mission->getCountry()));
                    $mission->content = $entityManager->getRepository(Content::class)->findOneBy(['mission_id' => $mission->getId(), 'lang' => $lang]);
                }
            }
        } else {
            return $this->sendError('Pays inconnu ...');
        }

        return $this->render('pages/country.html.twig', [
            'country' => $country,
            'missions' => $missions,
            'blocks' => $blocks,
            'general' => $this->contentService->getGeneral($entityManager, $request->getLocale())
        ]);
    }

    private function getPageBlock($locale, $entity)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $display = [
            'newsletter' => $entity->getPageDisplayNewsletter(),
            'map' => $entity->getPageDisplayMap(),
            'posts' => $entity->getPageDisplayPost(),
            'missions' => $entity->getPageDisplayMission()
        ];

        // Block Map
        if ($display['map']) {
            // Getting countries
            $countries = $entityManager->getRepository(Country::class)->findBy([], ['id' => 'DESC']);
            $tabCountries = [];
            if (isset($countries) && !empty($countries)) {
                foreach ($countries as &$count) {
                    $count->content = $entityManager->getRepository(ContentCountry::class)->findOneBy(['country_id' => $count->getId(), 'lang' => $locale], ['id' => 'DESC']);
                    $nbMissions = $entityManager->getRepository(Mission::class)->findBy(['country' => $count->getCountry()]);
                    $tabCountries[$count->getCountry()] = [
                        'name' => $this->countryService->getCountryLabelToDisplay($count->getCountry()),
                        'image' => "/uploads/img_article/" . $count->getImage(),
                        'link' => $this->generateUrl('country', ['id' => $count->getId()]),
                        'missions' => sizeof($nbMissions),
                        'content' => $count->content->getShortContent()
                    ];
                }
                $display['map'] = $tabCountries;
            } else {
                $display['map'] = false;
            }
        }

        // block Posts
        if ($display['posts']) {
            // Getting posts
            $posts = $entityManager->getRepository(Post::class)->findBy([], ['id' => 'DESC'], 5);
            if (isset($posts) && !empty($posts)) {
                foreach ($posts as &$post) {
                    // set content from locale
                    $post->content = $entityManager->getRepository(Content::class)->findOneBy(['post_id' => $post->getId(), 'lang' => $locale]);
                    $post->link = $this->generateUrl('post', ['id' => $post->getId()]);
                }
                $display['posts'] = $posts;
            } else {
                $display['posts'] = false;
            }
        }

        // Block Missions
        if ($display['missions']) {

            // get related mission based on country
            if ($entity instanceof Country || $entity instanceof Mission) {
                $args = [
                    'country' => $entity->getCountry()
                ];
            } else {
                $args = [];
            }

            // Getting related missions
            $missions = $entityManager->getRepository(Mission::class)->findBy($args, ['id' => 'DESC'], 5);
            if (isset($missions) && !empty($missions)) {
                // set content from locale
                foreach ($missions as &$mission) {
                    $mission->setCountry($this->countryService->getCountryLabelToDisplay($mission->getCountry()));
                    $mission->content = $entityManager->getRepository(Content::class)->findOneBy(['mission_id' => $mission->getId(), 'lang' => $locale]);
                }
                $display['missions'] = $missions;
            } else {
                $display['missions'] = false;
            }
        }

        return $display;
    }

    private function sendError($error)
    {
        throw $this->createNotFoundException($error);
    }
}
