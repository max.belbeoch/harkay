<?php

namespace App\Controller;

use App\Entity\Content;
use App\Entity\Post;
use DateTime;
use DateTimeZone;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\PostFormType;
use App\Service\ContentService;
use App\Service\FileService;

class AdminPostController extends AbstractController
{

    /**
     * @Route("/admin/article/ajouter", name="admin_add_post")
     */
    public function addPost(Request $request, FileService $fileService, ContentService $contentService): Response
    {

        $post = new Post();

        $form = $this->createForm(PostFormType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $image = $form->get('image')->getData();

            $post = $form->getData();

            if ($image) {
                $newFileName = $fileService->uploadImage($image);
                $post->setImage($newFileName);
            }

            $post->setAuthor($this->getUser()->getId());
            $post->setCreatedAt(new DateTime("now", new DateTimeZone('GMT+2')));
            $post->setLastUpdatedAt(new DateTime("now", new DateTimeZone('GMT+2')));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($post);
            $entityManager->flush();

            foreach (Content::LANG as $key => $lang) {
                $content = new Content();
                $content->setLang($key);
                $content->setPostId($post->getId());
                $contentService->setContent($content, $form, true);
                $entityManager->persist($content);
            }

            $entityManager->flush();

            return $this->redirectToRoute('admin_posts');
        }

        return $this->render('admin/add_article.html.twig', [
            'form_post' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/article/modifier/{id}", name="admin_update_post")
     */
    public function updatePost(Request $request, FileService $fileService, ContentService $contentService, $id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $post = $entityManager->getRepository(Post::class)->findOneBy(['id' => $id]);
        $contents = $entityManager->getRepository(Content::class)->findAll(['post_id' => $post->getId()], false);
        $error = false;

        if ($post != null) {

            $form = $this->createForm(PostFormType::class, $post);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $image = $form->get('image')->getData();

                $postForm = $form->getData();

                if ($image) {
                    $newFileName = $fileService->uploadImage($image);
                    $postForm->setImage($newFileName);
                }

                $postForm->setLastUpdatedAt(new DateTime("now", new DateTimeZone('GMT+2')));

                foreach ($contents as $content) {
                    $contentService->setContent($content, $form);
                    $entityManager->persist($content);
                }

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($postForm);
                $entityManager->flush();

                return $this->redirectToRoute('admin_posts');
            }
        } else {
            $error = 'Article inconnu';
        }

        if (!$form->isSubmitted()) {
            $contentService->setFormContent($form, $contents);
        }

        return $this->render('admin/update_article.html.twig', [
            'error' => $error,
            'form_post' => $form->createView(),
        ]);
    }
}
