<?php

namespace App\Form;

use App\Entity\Country;
use App\Form\Type\LangType;
use App\Form\Type\CountryType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class CountryFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('country', CountryType::class, [
                'label' => 'Pays',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Le pays est obligatoire',
                    ]),
                ]
            ])
            ->add('image', FileType::class, [
                'label' => 'Image principale',
                'required' => false,
                'mapped' => false,
                'constraints' => [
                    new File([
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/gif',
                            'image/png',
                            'image/tiff',
                        ],
                        'mimeTypesMessage' => 'Format non reconnu (JPEG, GIF, PNG, TIFF)',
                    ]),
                ],
            ])
            // FR Content
            ->add('page_title_fr', TextType::class, [
                'label' => 'Titre SEO par défaut (fr)',
                'mapped' => false,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('page_description_fr', TextType::class, [
                'label' => 'Description SEO par défaut (fr)',
                'mapped' => false,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('title_fr', TextType::class, [
                'label' => 'Titre par défaut (fr)',
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Le titre / nom du pays Français est obligatoire',
                    ]),
                ]
            ])
            ->add('short_content_fr', TextType::class, [
                'label' => 'Description courte par défaut (fr)',
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'La description courte Française est obligatoire',
                    ]),
                ]
            ])
            ->add('content_1_fr', CKEditorType::class, [
                'label' => 'Population (fr)',
                'mapped' => false,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('content_2_fr', CKEditorType::class, [
                'label' => 'Culture 2 (fr)',
                'mapped' => false,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('content_3_fr', CKEditorType::class, [
                'label' => 'Protéger 3 (fr)',
                'mapped' => false,
                'required' => false,
                'empty_data' => ''
            ])
            // EN Content
            ->add('page_title_en', TextType::class, [
                'label' => 'Titre SEO (en)',
                'mapped' => false,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('page_description_en', TextType::class, [
                'label' => 'Description de la page (en)',
                'mapped' => false,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('title_en', TextType::class, [
                'label' => 'Titre (en)',
                'mapped' => false,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('short_content_en', TextType::class, [
                'label' => 'Description courte (en)',
                'mapped' => false,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('content_1_en', CKEditorType::class, [
                'label' => 'Population 1 (en)',
                'mapped' => false,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('content_2_en', CKEditorType::class, [
                'label' => 'Culture 2 (en)',
                'mapped' => false,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('content_3_en', CKEditorType::class, [
                'label' => 'Protéger 3 (en)',
                'mapped' => false,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('published_en', CheckboxType::class, [
                'label' => 'Publier le contenu Anglais sur le site',
                'required' => false,
                'mapped' => false,
            ])
            // ES Content
            ->add('page_title_es', TextType::class, [
                'label' => 'Titre SEO (es)',
                'mapped' => false,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('page_description_es', TextType::class, [
                'label' => 'Description de la page (es)',
                'mapped' => false,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('title_es', TextType::class, [
                'label' => 'Titre (es)',
                'mapped' => false,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('short_content_es', TextType::class, [
                'label' => 'Description courte (es)',
                'mapped' => false,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('content_1_es', CKEditorType::class, [
                'label' => 'Population 1 (es)',
                'mapped' => false,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('content_2_es', CKEditorType::class, [
                'label' => 'Culture 2 (es)',
                'mapped' => false,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('content_3_es', CKEditorType::class, [
                'label' => 'Protéger 3 (es)',
                'mapped' => false,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('published_es', CheckboxType::class, [
                'label' => 'Publier le contenu Espagnol sur le site',
                'required' => false,
                'mapped' => false,
            ])
            // Page display options
            ->add('page_display_newsletter', CheckboxType::class, [
                'label' => 'Afficher la newsletter',
                'required' => false,
            ])
            ->add('page_display_post', CheckboxType::class, [
                'label' => 'Afficher les derniers articles',
                'required' => false,
            ])
            ->add('page_display_mission', CheckboxType::class, [
                'label' => 'Afficher les dernières missions',
                'required' => false,
            ])
            ->add('page_display_map', CheckboxType::class, [
                'label' => 'Afficher la carte',
                'required' => false,
            ])
            ->add('save', SubmitType::class, ['label' => 'Enregistrer le pays']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Country::class,
        ]);
    }
}
