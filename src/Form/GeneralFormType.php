<?php

namespace App\Form;

use App\Entity\General;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class GeneralFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // FR Content
            ->add('page_title_fr', TextType::class, [
                'label' => 'Titre SEO par défaut (fr)',
                'mapped' => true,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('page_description_fr', TextType::class, [
                'label' => 'Description SEO par défaut (fr)',
                'mapped' => true,
                'required' => false,
                'empty_data' => ''
            ])
            // EN Content
            ->add('page_title_en', TextType::class, [
                'label' => 'Titre SEO (en)',
                'mapped' => true,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('page_description_en', TextType::class, [
                'label' => 'Description SEO (en)',
                'mapped' => true,
                'required' => false,
                'empty_data' => ''
            ])
            // ES Content
            ->add('page_title_es', TextType::class, [
                'label' => 'Titre SEO (es)',
                'mapped' => true,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('page_description_es', TextType::class, [
                'label' => 'Description SEO (es)',
                'mapped' => true,
                'required' => false,
                'empty_data' => ''
            ])

            ->add('legal_name', TextType::class, [
                'label' => 'Nom légal',
                'mapped' => true,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('siret', TextType::class, [
                'label' => 'N° de SIRET',
                'mapped' => true,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('country', TextType::class, [
                'label' => 'Pays',
                'mapped' => true,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('city', TextType::class, [
                'label' => 'Ville',
                'mapped' => true,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('zip_code', TextType::class, [
                'label' => 'Code postal',
                'mapped' => true,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('address', TextType::class, [
                'label' => 'Numéro et rue',
                'mapped' => true,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('email', TextType::class, [
                'label' => 'Adresse e-mail',
                'mapped' => true,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('phone_number', TextType::class, [
                'label' => 'Téléphone',
                'mapped' => true,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('donation_link', TextType::class, [
                'label' => 'Lien pour faire un don',
                'mapped' => true,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('fb_link', TextType::class, [
                'label' => 'Facebook',
                'mapped' => true,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('twt_link', TextType::class, [
                'label' => 'Twitter',
                'mapped' => true,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('ig_link', TextType::class, [
                'label' => 'Instagram',
                'mapped' => true,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('yt_link', TextType::class, [
                'label' => 'Youtube',
                'mapped' => true,
                'required' => false,
                'empty_data' => ''
            ])
            
            ->add('save', SubmitType::class, ['label' => 'Mettre à jour']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => General::class,
        ]);
    }
}
