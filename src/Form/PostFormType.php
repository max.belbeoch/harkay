<?php

namespace App\Form;

use App\Entity\Post;
use App\Form\Type\LangType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class PostFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('image', FileType::class, [
                'label' => 'Image principale',
                'required' => false,
                'mapped' => false,
                'constraints' => [
                    new File([
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/gif',
                            'image/png',
                            'image/tiff',
                        ],
                        'mimeTypesMessage' => 'Format non reconnu (JPEG, GIF, PNG, TIFF)',
                    ]),
                ],
            ])
            // FR Content
            ->add('page_title_fr', TextType::class, [
                'label' => 'Titre SEO par défaut (fr)',
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Le titre SEO est obligatoire',
                    ]),
                ]
            ])
            ->add('page_description_fr', TextareaType::class, [
                'label' => 'Description SEO par défaut (fr)',
                'mapped' => false,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('title_fr', TextType::class, [
                'label' => 'Titre par défaut (fr)',
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Le titre Français est obligatoire',
                    ]),
                ]
            ])
            ->add('short_content_fr', TextareaType::class, [
                'label' => 'Description courte par défaut (fr)',
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'La description courte Française est obligatoire',
                    ]),
                ]
            ])
            ->add('content_fr', CKEditorType::class, [
                'label' => 'Contenu par défaut (fr)',
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Le contenu Français est obligatoire',
                    ]),
                ]
            ])
            // EN Content
            ->add('page_title_en', TextType::class, [
                'label' => 'Titre SEO (en)',
                'mapped' => false,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('page_description_en', TextareaType::class, [
                'label' => 'Description SEO (en)',
                'mapped' => false,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('title_en', TextType::class, [
                'label' => 'Titre (en)',
                'mapped' => false,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('short_content_en', TextareaType::class, [
                'label' => 'Description courte (en)',
                'mapped' => false,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('content_en', CKEditorType::class, [
                'label' => 'Contenu (en)',
                'mapped' => false,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('published_en', CheckboxType::class, [
                'label' => 'Publier le contenu Anglais sur le site',
                'required' => false,
                'mapped' => false,
            ])
            // ES Content
            ->add('page_title_es', TextType::class, [
                'label' => 'Titre SEO (es)',
                'mapped' => false,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('page_description_es', TextareaType::class, [
                'label' => 'Description SEO (es)',
                'mapped' => false,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('title_es', TextType::class, [
                'label' => 'Titre (es)',
                'mapped' => false,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('short_content_es', TextareaType::class, [
                'label' => 'Description courte (es)',
                'mapped' => false,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('content_es', CKEditorType::class, [
                'label' => 'Contenu (es)',
                'mapped' => false,
                'required' => false,
                'empty_data' => ''
            ])
            ->add('published_es', CheckboxType::class, [
                'label' => 'Publier le contenu Espagnol sur le site',
                'required' => false,
                'mapped' => false,
            ])
            // Page display options
            ->add('page_display_newsletter', CheckboxType::class, [
                'label' => 'Afficher la newsletter',
                'required' => false,
            ])
            ->add('page_display_post', CheckboxType::class, [
                'label' => 'Afficher les derniers articles',
                'required' => false,
            ])
            ->add('page_display_mission', CheckboxType::class, [
                'label' => 'Afficher les dernières missions',
                'required' => false,
            ])
            ->add('page_display_map', CheckboxType::class, [
                'label' => 'Afficher la carte',
                'required' => false,
            ])
            ->add('save', SubmitType::class, ['label' => 'Enregistrer l\'article']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Post::class,
        ]);
    }
}
