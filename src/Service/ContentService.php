<?php

namespace App\Service;

use App\Entity\Content;
use App\Entity\General;
use DateTime;
use DateTimeZone;


class ContentService
{

    public function __construct()
    {
    }

    // For Post and Mission

    public function setContent(&$content, $form, $create = false)
    {
        $lang = strtolower($content->getLang());
        
        $content->setPageTitle($form->get('page_title_' . $lang)->getData());
        $content->setPageDescription($form->get('page_description_' . $lang)->getData());
        $content->setTitle($form->get('title_' . $lang)->getData());
        $content->setShortContent($form->get('short_content_' . $lang)->getData());
        $content->setContent($form->get('content_' . $lang)->getData());
        $content->setLastUpdatedAt(new DateTime("now", new DateTimeZone('GMT+2')));

        if ($create) {
            $content->setCreatedAt(new DateTime("now", new DateTimeZone('GMT+2')));
        }

        // French published by default
        if ($lang != 'fr') {
            $content->setPublished($form->get('published_' . $lang)->getData());
        }
    }

    public static function setFormContent(&$form, $contents)
    {
        foreach ($contents as $content) {

            $lang = strtolower($content->getLang());
            $form->get('page_title_' . $lang)->setData($content->getPageTitle());
            $form->get('page_description_' . $lang)->setData($content->getPageDescription());
            $form->get('title_' . $lang)->setData($content->getTitle());
            $form->get('short_content_' . $lang)->setData($content->getShortContent());
            $form->get('content_' . $lang)->setData($content->getContent());

            // French published by default
            if ($lang != 'fr') {
                $form->get('published_' . $lang)->setData($content->getPublished());
            }
        }
    }

    // For country

    public function setCountryContent(&$content, $form, $create = false)
    {
        $lang = strtolower($content->getLang());
        
        $content->setPageTitle($form->get('page_title_' . $lang)->getData());
        $content->setPageDescription($form->get('page_description_' . $lang)->getData());
        $content->setTitle($form->get('title_' . $lang)->getData());
        $content->setShortContent($form->get('short_content_' . $lang)->getData());
        $content->setContent1($form->get('content_1_' . $lang)->getData());
        $content->setContent2($form->get('content_2_' . $lang)->getData());
        $content->setContent3($form->get('content_3_' . $lang)->getData());
        $content->setLastUpdatedAt(new DateTime("now", new DateTimeZone('GMT+2')));

        if ($create) {
            $content->setCreatedAt(new DateTime("now", new DateTimeZone('GMT+2')));
        }

        // French published by default
        if ($lang != 'fr') {
            $content->setPublished($form->get('published_' . $lang)->getData());
        }
    }

    public static function setFormCountryContent(&$form, $contents)
    {
        foreach ($contents as $content) {

            $lang = strtolower($content->getLang());
            $form->get('page_title_' . $lang)->setData($content->getPageTitle());
            $form->get('page_description_' . $lang)->setData($content->getPageDescription());
            $form->get('title_' . $lang)->setData($content->getTitle());
            $form->get('short_content_' . $lang)->setData($content->getShortContent());
            $form->get('content_1_' . $lang)->setData($content->getContent1());
            $form->get('content_2_' . $lang)->setData($content->getContent2());
            $form->get('content_3_' . $lang)->setData($content->getContent3());

            // French published by default
            if ($lang != 'fr') {
                $form->get('published_' . $lang)->setData($content->getPublished());
            }
        }
    }

    public function getGeneral($entityManager, $lang) {
        $general = $entityManager->getRepository(General::class)->findOneBy(['id' => 1]);
        if (isset($general) && !empty($general)) {
            $general = $general;
        } else {
            $general = [];
        }
        return $general;
    }
}
